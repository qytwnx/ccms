import Toast from "../../miniprogram_npm/@vant/weapp/toast/toast"
const api = require('../../utils/api').API

// pages/p-space/index.ts
Page({

    /**
     * 页面的初始数据
     */
    data: {
        userId: 0,
        userInfo: null,
        showPage: false
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options: { userId: any }) {
        this.setData({
            userId: options.userId
        })
        wx.showLoading({
            title: '加载中...',
        })
        if (this.data.userId === 0) {
            Toast.success('未知错误')
            setTimeout(function () {
                wx.navigateBack({
                    delta: 1
                })
            }, 500)
        }
        const params = {
            id: Number(this.data.userId)
        }
        api.findUserInfoApi(params).then((res: { data: { code: number; data: any; message: string }}) => {
            if (res.data.code == 200) {
                console.log(res.data.data);
                
                this.setData({
                    userInfo: res.data.data,
                    showPage: true
                })
                wx.hideLoading()
            } else {
                wx.hideLoading()
                Toast.success(res.data.message)
                setTimeout(function () {
                    wx.navigateBack({
                        delta: 1
                    })
                }, 500)
            }
        })

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})