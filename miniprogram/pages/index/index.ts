// index.ts
import Toast from '../../miniprogram_npm/@vant/weapp/toast/toast'
const api = require('../../utils/api').API
Page({
    data: {
        userId: 0,
        token: '',
        isReportedTemperature: '0'
    },
    onShow() {
        this.getTabBar().init()
        this.onLoad()
    },
    onLoad: function () {
        this.loadInfo()
        this.loadIsReportedTemperature()
    },

    loadInfo: function() {
        const that = this
        try {
            this.setData({
                token: wx.getStorageSync('token')
            })
        } catch (e) {
            Toast.fail('请登录')
            setTimeout(function () {
                that.toLogin()
            }, 500)
            return
        }

        if (this.data.token === '') {
            Toast.fail('请登录')
            setTimeout(function () {
                that.toLogin()
            }, 500)
            return
        }

        try {
            this.setData({
                userId: JSON.parse(wx.getStorageSync('userInfo')).id
            })
        } catch (e) {
            Toast.fail('请登录')
            setTimeout(function () {
                that.toLogin()
            }, 500)
            return
        }
    },

    loadIsReportedTemperature: function() {
        const params = {
            userId: this.data.userId
        }
        api.getTemplateTodayByUserId(params).then((res: { data: { code: number; data: any; message: string } }) => {
            if (res.data.code == 200) {
                this.setData({
                    isReportedTemperature: res.data.data.isReported
                })
            } else {
                Toast.fail(res.data.message);
            }
        }).catch((err: any) => {
            console.log(err);
        })
    },
    toLogin: function () {
        wx.navigateTo({
            url: '/pages/auth/login/index'
        })
    },
    toReportTemperature: function() {
        
        if (this.data.isReportedTemperature === '1') {
            Toast('今日体温已上报')
            return
        }
        wx.navigateTo({
            url: '/pages/r-temperature/index'
        })
    },
})

