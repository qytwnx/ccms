// pages/goods/goods.ts
Page({

    /**
     * 页面的初始数据
     */
    data: {
        mainActiveIndex: 0,
        activeId: [],
        items: [
            {
                text: '粮油调味',
                badge: 3,
                // 禁用选项
                disabled: false,
            },
            {
                text: '食品饮料',
                badge: 4,
                // 禁用选项
                disabled: false,
            },
            {
                text: '生鲜果蔬',
                badge: 4,
                // 禁用选项
                disabled: false,
            },
            {
                text: '个人护理',
                badge: 4,
                // 禁用选项
                disabled: false,
            },
        ],
        itemsContent: [
            [
                {
                    goodsName: '商品一',
                    goodsDescription: '商品描述',
                    goodsPrice: '2.00',
                    coverUrl: 'https://img01.yzcdn.cn/vant/ipad.jpeg'
                },
                {
                    goodsName: '商品二',
                    goodsDescription: '商品描述',
                    goodsPrice: '2.00',
                    coverUrl: 'https://img01.yzcdn.cn/vant/ipad.jpeg'
                }
            ],
            [
                {
                    goodsName: '商品1',
                    goodsDescription: '商品描述',
                    goodsPrice: '2.00',
                    coverUrl: 'https://img01.yzcdn.cn/vant/ipad.jpeg'
                },
                {
                    goodsName: '商品2',
                    goodsDescription: '商品描述',
                    goodsPrice: '2.00',
                    coverUrl: 'https://img01.yzcdn.cn/vant/ipad.jpeg'
                }
            ],
            [
                {
                    goodsName: '商品yi',
                    goodsDescription: '商品描述',
                    goodsPrice: '2.00',
                    coverUrl: 'https://img01.yzcdn.cn/vant/ipad.jpeg'
                },
                {
                    goodsName: '商品er',
                    goodsDescription: '商品描述',
                    goodsPrice: '2.00',
                    coverUrl: 'https://img01.yzcdn.cn/vant/ipad.jpeg'
                }
            ],
            [
                {
                    goodsName: '商品y',
                    goodsDescription: '商品描述',
                    goodsPrice: '2.00',
                    coverUrl: 'https://img01.yzcdn.cn/vant/ipad.jpeg'
                },
                {
                    goodsName: '商品e',
                    goodsDescription: '商品描述',
                    goodsPrice: '2.00',
                    coverUrl: 'https://img01.yzcdn.cn/vant/ipad.jpeg'
                }
            ]
        ]

    },


    onClickNav(e: {detail: {index: number}}) {
        this.setData({
          mainActiveIndex: e.detail.index || 0,
        });
      },

      onChange(event: { detail: any; }) {
        console.log(event.detail);
      },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad() {

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {
        this.getTabBar().init()
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})