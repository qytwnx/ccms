// pages/r-temperature/index.ts
import Toast from '../../miniprogram_npm/@vant/weapp/toast/toast';
import Notify from '../../miniprogram_npm/@vant/weapp/notify/notify'
const api = require('../../utils/api').API
Page({

    /**
     * 页面的初始数据
     */
    data: {
        bodyTemperature: '',
        isCough: '1',
        isDiarrhea: '1',
        isWeak: '1',
        otherSituation: '',
        agree: false,
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad() {

    },
    onAgreeChange: function () {
        this.setData({
            agree: !this.data.agree
        })
    },
    checkData: function () {
        if (this.data.bodyTemperature.length === 0) {
            Notify({ type: 'danger', message: '请输入体温信息' });
            return false
        }
        if (Number(this.data.bodyTemperature) < 0) {
            Notify({ type: 'danger', message: '体温不能小于0' });
            return false
        }
        if (Number(this.data.bodyTemperature) > 50) {
            Notify({ type: 'danger', message: '体温不能大于50' });
            return false
        }
        if (this.data.isCough !== '0' && this.data.isCough !== '1') {
            Notify({ type: 'danger', message: '是否咳嗽选择异常' });
            return false
        }
        if (this.data.isDiarrhea !== '0' && this.data.isDiarrhea !== '1') {
            Notify({ type: 'danger', message: '是否腹泻选择异常' });
            return false
        }
        if (this.data.isWeak !== '0' && this.data.isWeak !== '1') {
            Notify({ type: 'danger', message: '是否乏力选择异常' });
            return false
        }
        if (this.data.otherSituation.length === 0) {
            Notify({ type: 'danger', message: '请输入其他情况' });
            return false
        }
        if (!this.data.agree) {
            Notify({ type: 'danger', message: '请确认数据的准确性，并选择本人确认以上填报数据准确无误' });
            return false
        }

        return true

    },
    saveTemperature: function () {
        if (!this.checkData()) {
            return
        }
        const temperatureData = {
            bodyTemperature: this.data.bodyTemperature,
            isCough: this.data.isCough,
            isDiarrhea: this.data.isDiarrhea,
            isWeak: this.data.isWeak,
            otherSituation: this.data.otherSituation,
        }
        wx.showLoading({
            title: '加载中...',
        })
        api.insertTemperature(temperatureData).then((res: { data: { code: number; data: any; message: string } }) => {
            if (res.data.code == 200) {
                wx.hideLoading()
                Toast.success(res.data.message)
                setTimeout(function () {
                    wx.navigateBack({
                        delta: 1
                    })
                }, 500)
            } else {
                wx.hideLoading()
                Toast.fail(res.data.message);
            }
        }).catch((err: any) => {
            wx.hideLoading()
            console.log(err);
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})