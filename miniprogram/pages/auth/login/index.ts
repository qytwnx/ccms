// pages/auth/login/index.ts
const api = require('../../../utils/api').API
// import Toast from '../../../@vant/weapp/toast/toast'
import Toast from '../../../miniprogram_npm/@vant/weapp/toast/toast'
Page({

    /**
     * 页面的初始数据
     */
    data: {
        phoneNo: '',
        password: '',
        phoneError: '',
        passwordError: '',
        agreeUse: false,
        showPage: false,
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function () {
        const that = this
        Toast.loading({
            message: '加载中...',
            forbidClick: true,
            duration: 0,
            loadingType: 'spinner',
        });
        wx.getStorage({
            key: 'token',
            success(res) {
                if (res.errMsg === 'getStorage:ok') {
                    Toast.clear()
                    wx.reLaunch({
                        url: '/pages/mine/mine'
                    })
                } else {
                    that.setData({
                        showPage: true
                    })
                    Toast.clear()
                }
            },
            fail(err) {
                console.log(err);
                that.setData({
                    showPage: true
                })
                Toast.clear()
            }
        })
    },

    toRegister: function () {
        wx.navigateTo({
            url: '/pages/auth/register/index'
        })
    },
    forgetPassword: function () {
        // wx.navigateTo({
        //     url: '/pages/auth/f-password/index'
        // })
        Toast('请联系管理员重置密码')
    },

    login: function () {
        this.setData({
            phoneError: '',
            passwordError: ''
        })
        if (this.data.phoneNo === '') {
            this.setData({
                phoneError: '手机号不能为空'
            })
            return
        }
        const checkPhone = /^(?:(?:\+|00)86)?1\d{10}$/
        if (!checkPhone.test(this.data.phoneNo)) {
            this.setData({
                phoneError: '手机号格式错误'
            })
            return
        }
        if (this.data.password === '') {
            this.setData({
                passwordError: '密码不能为空'
            })
            return
        }
        if (this.data.password.length < 8 || this.data.password.length > 16) {
            this.setData({
                passwordError: '密码长度为8-16'
            })
            return
        }
        if (!this.data.agreeUse) {
            Toast('请同意微社区的使用协议')
            return
        }
        const loginData = {
            phoneNo: this.data.phoneNo,
            password: this.data.password
        }
        api.loginApi(loginData).then((res: { data: { code: number; data: { userInfo: any; token: string }; message: string } }) => {
            if (res.data.code == 200) {
                wx.setStorage({
                    key: "userInfo",
                    data: JSON.stringify(res.data.data.userInfo)
                })
                wx.setStorage({
                    key: "token",
                    data: res.data.data.token
                })
                Toast.success('登录成功');
                setTimeout(function () {
                    wx.reLaunch({
                        url: '/pages/mine/mine'
                    })
                }, 500)
            } else {
                Toast.fail(res.data.message);
            }
        }).catch((err: any) => {
            console.log(err);
        })
    },
    onagreeUseChange: function () {
        this.setData({
            agreeUse: !this.data.agreeUse
        })
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})