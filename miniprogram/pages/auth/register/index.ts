// pages/auth/register/index.ts
import Toast from '../../../miniprogram_npm/@vant/weapp/toast/toast'
const api = require('../../../utils/api').API
Page({

    /**
     * 页面的初始数据
     */
    data: {
        phoneNo: '',
        phoneError: '',
        password: '',
        passwordError: '',
        rePassword: '',
        rePasswordError: '',
        userName: '',
        userNameError: '',
        code: '',
        agreeUse: false,
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad() {

    },

    checkData: function() {
        this.setData({
            phoneError: '',
            passwordError: '',
            rePasswordError: '',
            userNameError: ''
        })

        if (this.data.phoneNo === '') {
            this.setData({
                phoneError: '手机号不能为空'
            })
            return false
        }
        const checkPhone = /^(?:(?:\+|00)86)?1\d{10}$/
        if (!checkPhone.test(this.data.phoneNo)) {
            this.setData({
                phoneError: '手机号格式错误'
            })
            return false
        }
        if (this.data.password === '') {
            this.setData({
                passwordError: '密码不能为空'
            })
            return false
        }
        if (this.data.password.length < 8 || this.data.password.length > 16) {
            this.setData({
                passwordError: '密码长度为8-16'
            })
            return false
        }
        if (this.data.password !== this.data.rePassword) {
            this.setData({
                rePasswordError: '两次输入密码不一致'
            })
            return false
        }
        if (this.data.userName === '') {
            this.setData({
                userNameError: '姓名不能为空'
            })
            return false
        }
        if (this.data.userName.length < 1 || this.data.userName.length > 10) {
            this.setData({
                userNameError: '姓名长度为1-10'
            })
            return false
        }
        return true
    },

    register: function() {
        if (!this.checkData()) {
            return
        }
        if (!this.data.agreeUse) {
            Toast('请同意微社区的使用协议')
            return
        }
        const registerData = {
            phoneNo: this.data.phoneNo,
            userName: this.data.userName,
            password: this.data.password,
            rePassword: this.data.rePassword
        }
        api.registerApi(registerData).then((res: { data: { code: number; data: any; message: string } }) => {
            if (res.data.code == 200) {
                Toast.success(res.data.message)
                setTimeout(function () {
                    wx.navigateBack({
                        delta: 1
                      })
                }, 500)
            } else {
                Toast.fail(res.data.message);
            }
        }).catch((err: any) => {
            console.log(err);
        })
    },
    onagreeUseChange: function () {
        this.setData({
            agreeUse: !this.data.agreeUse
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})