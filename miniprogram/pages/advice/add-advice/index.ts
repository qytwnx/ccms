// pages/advice/add-advice/index.ts
import Toast from '../../../miniprogram_npm/@vant/weapp/toast/toast';
import Notify from '../../../miniprogram_npm/@vant/weapp/notify/notify'
import { serviceIp } from '../../../utils/request';
const api = require('../../../utils/api').API

Page({

    /**
     * 页面的初始数据
     */
    data: {
        title: '',
        type: '0',
        imageUrl: [] as unknown as object[],
        content: '',
        url: ''
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad() {

    },
    checkData: function () {
        if (this.data.title.length === 0) {
            Notify({ type: 'danger', message: '标题不能为空' });
            return false
        }
        if (this.data.title.length > 17) {
            Notify({ type: 'danger', message: '标题过长' });
            return false
        }
        if (this.data.type !== '0' && this.data.type !== '1') {
            Notify({ type: 'danger', message: '类型选择错误' });
            return false
        }
        if (this.data.content.length === 0) {
            Notify({ type: 'danger', message: '内容不能为空' });
            return false
        }
        if (this.data.content.length > 350) {
            Notify({ type: 'danger', message: '内容过长' });
            return false
        }
        return true
    },
    saveAdvice: function () {
        if (!this.checkData()) {
            return
        }
        const adviceDto = {
            title: this.data.title,
            type: this.data.type,
            content: this.data.content,
            imageUrl: this.data.url,
        }
        api.insertAdviceApi(adviceDto).then((res: { data: { code: number; data: any; message: string } }) => {
            if (res.data.code == 200) {
                Toast.success(res.data.message)
                setTimeout(function () {
                    wx.navigateBack({
                        delta: 1
                    })
                }, 500)
            } else {
                Toast.fail(res.data.message);
            }
        }).catch((err: any) => {
            console.log(err);
        })
    },

    afterRead(event: { detail: { file: any; }; }) {
        const { file } = event.detail;
        console.log(file.path);
        const that = this
        wx.uploadFile({
            url: serviceIp + 'files/upload', //仅为示例，非真实的接口地址
            filePath: file.url,
            name: 'file',
            header: {
                'content-type': 'application/json;charset=utf-8',
                'Authorization': wx.getStorageSync('token') ? 'Bearer ' + wx.getStorageSync('token') : null
            },
            success(res) {
                const data = JSON.parse(res.data)
                const obj = {
                    'url':String(data.data),
                    'isImage': true
                }
                var listT:object[] = [obj]
                if (data.code == 200) {
                    that.setData({
                        imageUrl: listT,
                        url: data.data
                    })
                }
            }
        })
    },

    delImage() {
        this.setData({
            imageUrl: [], 
            url: '' 
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {
    
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})