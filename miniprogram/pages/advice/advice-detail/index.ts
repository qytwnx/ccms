// pages/advice/advice-detail/index.ts
import Toast from '../../../miniprogram_npm/@vant/weapp/toast/toast'
const api = require('../../../utils/api').API
Page({

    /**
     * 页面的初始数据
     */
    data: {
        aid: null,
        advice: {
            id: 0,
            title: '',
            content: '',
            imageUrl: '',
            type: '',
            replyContent: '',
            createdAt: '',
            userInfo: {
                id: 0,
                userName: '',
                avatarUrl: '',
                phoneNo: ''
            }
        }
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options: { aid: any }) {
        this.setData({
            aid: options.aid
        })
        wx.showLoading({
            title: '加载中...',
        })
        if (this.data.aid === 0) {
            Toast.success('未知错误')
            setTimeout(function () {
                wx.navigateBack({
                    delta: 1
                })
            }, 500)
        }
        this.loadAdvice()
        wx.hideLoading()
    },
    loadAdvice() {
        const param = {
            id: this.data.aid
        }
        api.findAdviceByIdApi(param).then((res: { data: { code: number; data: any; message: string } }) => {
            if (res.data.code === 200) {
                this.setData({
                    advice: res.data.data
                })
            } else {
                Toast.fail(res.data.message)
            }
        }).catch((err: any) => {
            console.log(err);
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})