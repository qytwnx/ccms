import Toast from "../../miniprogram_npm/@vant/weapp/toast/toast"

// pages/advice/advice.ts
const api = require('../../utils/api').API
Page({

    /**
     * 页面的初始数据
     */
    data: {
        pageTip: '查看更多',
        pageTipShow: false,
        adviceList: [
            {
                id: 1,
                title: '标题标题标题标题标题标题标题标',
                type: '类型',
                content: '内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容',
                replyContent: '回复回复回复回复回复回复回复回复回复回复回复回复回复回复回复回复回复回复回复回复回复回复回复回复回复回复回复回复回复回复回复回复回复回复',
                createdAt: '2022-11-07 23:44'
            },
        ],
        pageNum: 1,
        pageSize: 10,
        keyword: '',
        total: 0,
        lastCount: 0
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad() {
        this.loadAdviceApi()
    },

    loadAdviceApi: function() {
        const params = {
            pageNum: Number(this.data.pageNum),
            pageSize: Number(this.data.pageSize),
            keyword: this.data.keyword
        }
        wx.showLoading({
            title: '加载中...',
        })
        api.advicePagesApi(params).then((res: { data: { code: number; data: any; message: string } }) => {
            if (res.data.code == 200) {
                if (this.data.pageNum == 1) {
                    let resCount = res.data.data.records.length
                    this.setData({
                        adviceList: res.data.data.records,
                        lastCount: resCount,
                        pageTipShow: true
                    })
                    wx.hideLoading()
                } else {
                    let oldList = this.data.adviceList
                    let tempList = res.data.data.records
                    let resCount = res.data.data.records.length
                    let newList = oldList.concat(tempList)
                    this.setData({
                        adviceList: newList,
                        lastCount: resCount,
                        pageTipShow: true
                    })
                    wx.hideLoading()
                }
            } else {
                Toast.fail(res.data.message);
            }
        }).catch((err: any) => {
            console.log(err);
        })
    },

    showAdviceDetail: function(event: { currentTarget: { dataset: { aid: number; }; }; }) {
        const aid = event.currentTarget.dataset.aid
        wx.navigateTo({
            url: '/pages/advice/advice-detail/index?aid=' + aid
        })
    },

    addAdvice: function () {
        wx.navigateTo({
            url: './add-advice/index'
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {
        this.getTabBar().init()
        this.setData({
            pageNum: 1,
            pageSize: 10,
            keyword: ''
        })
        this.loadAdviceApi()
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {
        if (this.data.lastCount == this.data.pageSize) {
            (this.data.pageNum)++
            this.loadAdviceApi()
        } else {
            this.setData({
                pageTip: '没有更多数据'
            })
        }
    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})