// pages/mine/c-admin-info/index.ts
const api = require('../../../utils/api').API
import Toast from '../../../miniprogram_npm/@vant/weapp/toast/toast'
Page({

    /**
     * 页面的初始数据
     */
    data: {
        adminList: []
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad() {
        api.findAdminAllApi().then((res: { data: { code: number; data: any; message: string } }) => {
            if (res.data.code === 200) {
                this.setData({
                    adminList: res.data.data
                })
            } else {
                Toast.fail(res.data.message)
            }
        }).catch((err: any) => {
            console.log(err);
        })
    },
})