// pages/mine/m-advice/index.ts
const api = require('../../../utils/api').API
import Toast from '../../../miniprogram_npm/@vant/weapp/toast/toast'
Page({

    /**
     * 页面的初始数据
     */
    data: {
        activeNames: ['1'],
        token: '',
        userId: null,
        adviceList: []
    },

    onChange(event: { detail: any; }) {
        this.setData({
            activeNames: event.detail,
        });
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad() {
        this.loadUserId()
        this.loadAdvicesByUserId()
    },

    loadUserId: function () {
        try {
            this.setData({
                token: wx.getStorageSync('token')
            })
        } catch (e) {
            Toast.fail('请登录')
            setTimeout(function () {
                wx.navigateTo({
                    url: '/pages/auth/login/index'
                })
            }, 500)
            return
        }

        if (this.data.token === '') {
            Toast.fail('请登录')
            setTimeout(function () {
                wx.navigateTo({
                    url: '/pages/auth/login/index'
                })
            }, 500)
            return
        }

        try {
            this.setData({
                userId: JSON.parse(wx.getStorageSync('userInfo')).id,
            })
        } catch (e) {
            Toast.fail('请登录')
            setTimeout(function () {
                wx.navigateTo({
                    url: '/pages/auth/login/index'
                })
            }, 500)
            return
        }
    },
    loadAdvicesByUserId: function() {
        const params = {
            id: this.data.userId
        }
        api.getAdvicesByUserId(params).then((res: { data: { code: number; data: any; message: string } }) => {
            if (res.data.code === 200) {
                this.setData({
                    adviceList: res.data.data
                })
            } else {
                Toast.fail(res.data.message)
            }
        }).catch((err: any) => {
            console.log(err);
        })
    },


    showAdviceDetail: function(event: { currentTarget: { dataset: { aid: number; }; }; }) {
        const aid = event.currentTarget.dataset.aid
        wx.navigateTo({
            url: '/pages/advice/advice-detail/index?aid=' + aid
        })
    },

    deleteAdvice: function(event: { currentTarget: { dataset: { aid: number; }; }; }) {
        const aid = event.currentTarget.dataset.aid
        const params = {
            id: Number(aid)
        }
        api.deleteAdviceApi(params).then((res: { data: { code: number; data: any; message: string } }) => {
            if (res.data.code === 200) {
                Toast.success('删除成功')
                this.loadAdvicesByUserId()
            } else {
                Toast.fail(res.data.message)
            }
        }).catch((err: any) => {
            console.log(err);
        })
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {
        console.log(111);
        this.loadAdvicesByUserId()
    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})