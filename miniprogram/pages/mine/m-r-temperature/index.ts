// pages/mine/m-r-temperature/index.ts
import Toast from "../../../miniprogram_npm/@vant/weapp/toast/toast"
const api = require('../../../utils/api').API
Page({

    /**
     * 页面的初始数据
     */
    data: {
        pageTip: '查看更多',
        pageTipShow: false,
        temperatureList: [],
        pageNum: 1,
        pageSize: 10,
        userId: '',
        token: '',
        total: 0,
        lastCount: 0
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad() {
        this.loadUserId()
        this.loadTemperatureApi()
    },
    loadUserId: function () {
        try {
            this.setData({
                token: wx.getStorageSync('token')
            })
        } catch (e) {
            Toast.fail('请登录')
            setTimeout(function () {
                wx.navigateTo({
                    url: '/pages/auth/login/index'
                })
            }, 500)
            return
        }

        if (this.data.token === '') {
            Toast.fail('请登录')
            setTimeout(function () {
                wx.navigateTo({
                    url: '/pages/auth/login/index'
                })
            }, 500)
            return
        }

        try {
            this.setData({
                userId: JSON.parse(wx.getStorageSync('userInfo')).id,
            })
        } catch (e) {
            Toast.fail('请登录')
            setTimeout(function () {
                wx.navigateTo({
                    url: '/pages/auth/login/index'
                })
            }, 500)
            return
        }
    },

    loadTemperatureApi: function() {
        const params = {
            pageNum: Number(this.data.pageNum),
            pageSize: Number(this.data.pageSize),
            userId: Number(this.data.userId)
        }
        wx.showLoading({
            title: '加载中...',
        })
        api.getTemperaturePageByUserId(params).then((res: { data: { code: number; data: any; message: string } }) => {
            if (res.data.code == 200) {
                if (this.data.pageNum == 1) {
                    let resCount = res.data.data.records.length
                    this.setData({
                        temperatureList: res.data.data.records,
                        lastCount: resCount,
                        pageTipShow: true
                    })
                    if (this.data.lastCount < this.data.pageSize) {
                        this.setData({
                            pageTip: '没有更多数据'
                        })
                    }
                    wx.hideLoading()
                } else {
                    let oldList = this.data.temperatureList
                    let tempList = res.data.data.records
                    let resCount = res.data.data.records.length
                    let newList = oldList.concat(tempList)
                    this.setData({
                        temperatureList: newList,
                        lastCount: resCount,
                        pageTipShow: true
                    })
                    if (this.data.lastCount < this.data.pageSize) {
                        this.setData({
                            pageTip: '没有更多数据'
                        })
                    }
                    wx.hideLoading()
                }
            } else {
                wx.hideLoading()
                Toast.fail(res.data.message);
            }
        }).catch((err: any) => {
            wx.hideLoading()
            console.log(err);
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {
        if (this.data.lastCount == this.data.pageSize) {
            (this.data.pageNum)++
            this.loadTemperatureApi()
        } else {
            this.setData({
                pageTip: '没有更多数据'
            })
        }
    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})