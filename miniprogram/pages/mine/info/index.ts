import { serviceIp } from "../../../utils/request"
import Toast from "../../../miniprogram_npm/@vant/weapp/toast/toast"
const api = require('../../../utils/api').API
// pages/mine/info/index.ts
Page({

    /**
     * 页面的初始数据
     */
    data: {
        userInfo: {
            id: null,
            userName: null,
            gender: null,
            userAge: null,
            phoneNo: null,
            userEmail: null,
            createdAt: null,
            address: null,
            avatarUrl: null,
            roleFlag: null
        },
        token: '',
        showPage: false,
        serviceIp: serviceIp
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad() {
        this.loadUserInfo()
    },
    loadUserInfo: function () {
        // const that = this
        try {
            this.setData({
                token: wx.getStorageSync('token')
            })
        } catch (e) {
            Toast.fail('请登录')
            setTimeout(function () {
                wx.navigateTo({
                    url: '/pages/auth/login/index'
                })
            }, 500)
            return
        }

        if (this.data.token === '') {
            Toast.fail('请登录')
            setTimeout(function () {
                wx.navigateTo({
                    url: '/pages/auth/login/index'
                })
            }, 500)
            return
        }

        try {
            this.setData({
                userInfo: JSON.parse(wx.getStorageSync('userInfo')),
                showPage: true
            })
        } catch (e) {
            Toast.fail('请登录')
            setTimeout(function () {
                wx.navigateTo({
                    url: '/pages/auth/login/index'
                })
            }, 500)
            return
        }
    },

    saveInfo: function () {
        console.log(this.data.userInfo);
        const userInfoDto = {
            userName: this.data.userInfo.userName,
            avatarUrl: this.data.userInfo.avatarUrl,
            phoneNo: this.data.userInfo.phoneNo,
            userEmail: this.data.userInfo.userEmail,
            gender: this.data.userInfo.gender,
            userAge: Number(this.data.userInfo.userAge),
            address: this.data.userInfo.address
        }
        api.updataUserInfoApi(userInfoDto).then((res: { data: { code: number; data: any; message: string } }) => {
            if (res.data.code === 200) {
                wx.setStorage({
                    key: "userInfo",
                    data: JSON.stringify(this.data.userInfo)
                })
                Toast.success(res.data.message)
            } else {
                Toast.success(res.data.message)
            }
        }).catch((err: any) => {
            console.log(err);
        })
    },
    onChange(event: { currentTarget: { dataset: { name: any } }; detail: any }) {
        var attributeName = 'userInfo.' + event.currentTarget.dataset.name
        this.setData({
            [attributeName]: event.detail,
        })
    },



    uploadAvatar: function () {
        const serviceIp = this.data.serviceIp
        const that = this
        wx.chooseImage({
            success(res) {
                const tempFilePaths = res.tempFilePaths
                wx.uploadFile({
                    url: serviceIp + 'files/upload', //仅为示例，非真实的接口地址
                    filePath: tempFilePaths[0],
                    name: 'file',
                    header: {
                        'content-type': 'application/json;charset=utf-8',
                        'Authorization': wx.getStorageSync('token') ? 'Bearer ' + wx.getStorageSync('token') : null
                    },
                    success(res) {
                        const data = JSON.parse(res.data)
                        if (data.code == 200) {
                            that.setData({
                                ['userInfo.avatarUrl']: data.data,
                            })
                        }
                    }
                })
            }
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})