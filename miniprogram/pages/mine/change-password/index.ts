// pages/mine/change-password/index.ts
import Toast from '../../../miniprogram_npm/@vant/weapp/toast/toast'
import Notify from '../../../miniprogram_npm/@vant/weapp/notify/notify'
const api = require('../../../utils/api').API
Page({

    /**
     * 页面的初始数据
     */
    data: {
        phoneNo: '',
        oldPassword: '',
        newPassword: '',
        reNewPassword: ''
    },
    checkData: function () {
        if (this.data.phoneNo.length === 0) {
            Notify({ type: 'danger', message: '手机号不能为空' });
            return false
        }
        const checkPhone = /^(?:(?:\+|00)86)?1\d{10}$/
        if (!checkPhone.test(this.data.phoneNo)) {
            Notify({ type: 'danger', message: '手机号格式错误' });
            return false
        }
        if (this.data.oldPassword.length === 0) {
            Notify({ type: 'danger', message: '旧密码不能为空' });
            return false
        }
        if (this.data.oldPassword.length < 8 || this.data.oldPassword.length > 16) {
            Notify({ type: 'danger', message: '密码长度为8-16' });
            return false
        }
        if (this.data.newPassword.length === 0) {
            Notify({ type: 'danger', message: '新密码不能为空' });
            return false
        }
        if (this.data.newPassword.length < 8 || this.data.newPassword.length > 16) {
            Notify({ type: 'danger', message: '密码长度为8-16' });
            return false
        }
        if (this.data.reNewPassword.length === 0) {
            Notify({ type: 'danger', message: '确认密码不能为空' });
            return false
        }
        if (this.data.reNewPassword.length < 8 || this.data.reNewPassword.length > 16) {
            Notify({ type: 'danger', message: '密码长度为8-16' });
            return false
        }

        if (this.data.newPassword !== this.data.reNewPassword) {
            Notify({ type: 'danger', message: '两次输入密码不一致' });
            return false
        }
        return true
    },
    changePassword: function() {
        if (!this.checkData()) {
            return
        }
        const pass = {
            phoneNo: this.data.phoneNo,
            oldPassword: this.data.oldPassword,
            newPassword: this.data.newPassword,
            reNewPassword: this.data.reNewPassword
        }
        api.changePasswordApi(pass).then((res: { data: { code: number; data: any; message: string } }) => {
            if (res.data.code == 200) {
                Toast.success(res.data.message)
                setTimeout(function () {
                    wx.navigateBack({
                        delta: 1
                      })
                }, 500)
            } else {
                Toast.fail(res.data.message);
            }
        }).catch((err: any) => {
            console.log(err);
        })
    },
})