// pages/mine/mine.ts
import Toast from '../../miniprogram_npm/@vant/weapp/toast/toast'
const api = require('../../utils/api').API
Page({

    /**
     * 页面的初始数据
     */
    data: {
        userInfo: null,
        userId: 0,
        token: '',
        showPage: false,
        isReportedTemperature: '0',
        isVolunteer: false
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function () {
        this.loadInfo()
        this.loadIsReportedTemperature()
        this.loadIsVolunteer()
    },

    loadInfo: function() {
        const that = this
        try {
            this.setData({
                token: wx.getStorageSync('token')
            })
        } catch (e) {
            Toast.fail('请登录')
            setTimeout(function () {
                that.toLogin()
            }, 500)
            return
        }

        if (this.data.token === '') {
            Toast.fail('请登录')
            setTimeout(function () {
                that.toLogin()
            }, 500)
            return
        }

        try {
            this.setData({
                userInfo: JSON.parse(wx.getStorageSync('userInfo')),
                userId: JSON.parse(wx.getStorageSync('userInfo')).id,
                showPage: true
            })
        } catch (e) {
            Toast.fail('请登录')
            setTimeout(function () {
                that.toLogin()
            }, 500)
            return
        }
    },

    loadIsVolunteer: function() {
        const params = {
            id: this.data.userId
        }
        api.getIsVolunteer(params).then((res: { data: { code: number; data: any; message: string } }) => {
            if (res.data.code == 200) {
                this.setData({
                    isVolunteer: res.data.data.flag
                })
                
            } else {
                Toast.fail(res.data.message);
            }
        }).catch((err: any) => {
            console.log(err);
        })
    },

    loadIsReportedTemperature: function() {
        const params = {
            userId: this.data.userId
        }
        api.getTemplateTodayByUserId(params).then((res: { data: { code: number; data: any; message: string } }) => {
            if (res.data.code == 200) {
                this.setData({
                    isReportedTemperature: res.data.data.isReported
                })
            } else {
                Toast.fail(res.data.message);
            }
        }).catch((err: any) => {
            console.log(err);
        })


    },

    toLogin: function () {
        wx.navigateTo({
            url: '/pages/auth/login/index'
        })
    },

    toLogout: function () {
        wx.removeStorageSync('token')
        wx.removeStorageSync('userInfo')
        Toast.success('成功退出登录')
        setTimeout(function () {
            wx.reLaunch({
                url: '/pages/auth/login/index'
            })
              
        }, 500)

    },

    inReport: function () {
        wx.navigateTo({
            url: '/pages/mine/m-in-report/index'
        })
    },
    outReport: function () {
        wx.navigateTo({
            url: '/pages/mine/m-out-report/index'
        })
    },
    reportTemperature: function () {
        wx.navigateTo({
            url: '/pages/mine/m-r-temperature/index'
        })
    },
    toReportTemperature: function() {
        wx.navigateTo({
            url: '/pages/r-temperature/index'
        })
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {
        this.getTabBar().init()
        this.onLoad()
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})