// pages/in-report/index.ts
import { serviceIp } from '../../utils/request';
const api = require('../../utils/api').API
import Toast from '../../miniprogram_npm/@vant/weapp/toast/toast'
Page({

    /**
     * 页面的初始数据
     */
    data: {
        name: '',
        startPlace: '',
        arrivalTime: '',
        bodyTemperature: '',
        bodyState: '',
        recentTrack: '',
        passEpidemicArea: '',
        entourage: '',
        phoneNo: '',
        detailAddress: '',
        nucleicAcidUrl: '',
        healthCode: '',
        imageUrl1: [] as unknown as object[],
        imageUrl2: [] as unknown as object[],
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad() {

    },
    saveReport() {
        const data = {
            name: this.data.name,
            startPlace: this.data.startPlace,
            arrivalTime: this.data.arrivalTime,
            bodyTemperature: this.data.bodyTemperature,
            bodyState: this.data.bodyState,
            recentTrack: this.data.recentTrack,
            passEpidemicArea: this.data.passEpidemicArea,
            entourage: this.data.entourage,
            phoneNo: this.data.phoneNo,
            detailAddress: this.data.detailAddress,
            nucleicAcidUrl: this.data.nucleicAcidUrl,
            healthCode: this.data.healthCode,
        }
        console.log(data);
        api.insertInApi(data).then((res: { data: { code: number; data: any; message: string } }) => {
            if (res.data.code == 200) {
                Toast.success(res.data.message)
                setTimeout(function () {
                    wx.navigateBack({
                        delta: 1
                    })
                }, 500)
            } else {
                Toast.fail(res.data.message);
            }
        }).catch((err: any) => {
            console.log(err);
        })
        
    },


    afterRead1(event: { detail: { file: any; }; }) {
        const { file } = event.detail;
        console.log(file.path);
        const that = this
        wx.uploadFile({
            url: serviceIp + 'files/upload', //仅为示例，非真实的接口地址
            filePath: file.url,
            name: 'file',
            header: {
                'content-type': 'application/json;charset=utf-8',
                'Authorization': wx.getStorageSync('token') ? 'Bearer ' + wx.getStorageSync('token') : null
            },
            success(res) {
                const data = JSON.parse(res.data)
                const obj = {
                    'url':String(data.data),
                    'isImage': true
                }
                var listT:object[] = [obj]
                if (data.code == 200) {
                    that.setData({
                        imageUrl1: listT,
                        nucleicAcidUrl: data.data
                    })
                }
            }
        })
    },

    delImage1() {
        this.setData({
            imageUrl1: [], 
            nucleicAcidUrl: '' 
        })
    },

    afterRead2(event: { detail: { file: any; }; }) {
        const { file } = event.detail;
        console.log(file.path);
        const that = this
        wx.uploadFile({
            url: serviceIp + 'files/upload', //仅为示例，非真实的接口地址
            filePath: file.url,
            name: 'file',
            header: {
                'content-type': 'application/json;charset=utf-8',
                'Authorization': wx.getStorageSync('token') ? 'Bearer ' + wx.getStorageSync('token') : null
            },
            success(res) {
                const data = JSON.parse(res.data)
                const obj = {
                    'url':String(data.data),
                    'isImage': true
                }
                var listT:object[] = [obj]
                if (data.code == 200) {
                    that.setData({
                        imageUrl2: listT,
                        healthCode: data.data
                    })
                }
            }
        })
    },

    delImage2() {
        this.setData({
            imageUrl2: [], 
            healthCode: '' 
        })
    }
})