// custom-tab-bar/index.ts
Component({
    /**
     * 组件的属性列表
     */
    properties: {

    },

    /**
     * 组件的初始数据
     */
    data: {
        active: 0,
        tabbars: [
            {
            url: "/pages/index/index",
            icon: "home-o",
            text: "首页",
        }, 
        {
            url: "/pages/advice/advice",
            icon: "records",
            text: "建议",
        },
        {
            url: "/pages/goods/goods",
            icon: "shop-o",
            text: "商品",
        },
        {
            url: "/pages/mine/mine",
            icon: "user-circle-o",
            text: "我的",
        }]
    },

    /**
     * 组件的方法列表
     */
    methods: {
        onChange: function(event: { detail: number; }) {
            var that = this;
            let tabbars = that.data.tabbars;
            that.setData({ active: event.detail });
            wx.switchTab({
                url: tabbars[event.detail].url,
            })
        },
        init() {
            const page: any = getCurrentPages().pop();
            this.setData({
                active: this.data.tabbars.findIndex(item => item.url === `/${page.route}`)
            });
        }
    }
})
