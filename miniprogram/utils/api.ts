
import request from './request'

const API = {
    // 注册
    registerApi: (data: any) => request("POST", `user/register`, data),
    // 登录
    loginApi: (data: any) => request("POST", `user/login`, data),
    // 更新学生信息
    updataUserInfoApi: (data: any) => request("POST", `user/update`, data),
    // 查找用户信息
    findUserInfoApi: (data: any) => request("GET", `user/find-one`, data),
    // 修改密码
    changePasswordApi: (data: any) => request("POST", `user/change-password`, data),
    // 获取管理员列表
    findAdminAllApi: () => request("GET", `admin/all`),


    /**
     * 建议
     */
    // 根据用户ID获取建议
    getAdvicesByUserId: (params: any) => request("GET", `advice/user-id`, params),
    // 新增建议
    insertAdviceApi: (data: any) => request("POST", `advice/insert`, data),
    // 建议page
    advicePagesApi: (data: any) => request("GET", `advice/pages`, data),
    // 删除建议
    deleteAdviceApi: (data: any) => request("GET", `advice/delete`, data),
    // id查询建议
    findAdviceByIdApi: (data: any) => request("GET", `advice/id`, data),


    /**
     * 体温
     */
    // 体温上报
    insertTemperature: (data: any) => request("POST", `temperature/insert`, data),
    // 根据用户id分页
    getTemperaturePageByUserId: (data: any) => request("GET", `temperature/user`, data),
    // 查询用户今日上报信息
    getTemplateTodayByUserId: (data: any) => request("GET", `temperature/user-today`, data),


    // 是否志愿者
    getIsVolunteer: (data: any) => request("GET", `volunteer/exist`, data),

    // 外出
    insertOutApi: (data: any) => request("POST", `out-report/insert`, data),
    // 返乡
    insertInApi: (data: any) => request("POST", `in-report/insert`, data),
    // // 获取文章列表
    // getArticleList: (data) => request("GET", `article/page/`, data),
    // // 获取文章详情
    // getArticleById: (data) => request("GET", `article/${data}`),
    // // 获取量表列表
    // getEvaluationList: (data) => request("GET", `evaluation/page`, data),
    // // 获取量表内容
    // getEvaluationDetail: (data) => request("GET", `evaluation-question/${data}`),
    // // 保存测评内容
    // saveEvaluationDetail: (data) => request("POST", `/reply-evaluation/`, data),
    // // 据学生id获取测评列表
    // getEvaluationListByStudentId: (data) => request("GET", `/reply-evaluation/selectByStudentId/${data}`),
    // // 据学生院系id获取量表列表
    // getEvaluationListByDepartmentId: (data) => request("GET", `/weekly-evaluation/studentGetPage`, data),
    // // 获取心理小知识
    // getSmallKnowledge: (data) => request("GET", `/small-knowledge/page`, data),
    // // 获取心理小知识详情
    // getSmallKnowledgeDetail: (data) => request("GET", `/small-knowledge/${data}`),
    // // 获取动态列表
    // getDynamicList: (data) => request("GET", `/dynamic/page`, data),
    // // 保存详情
    // getDynamicDetail: (data) => request("GET", `/dynamic/${data}`),
    // // 保存动态
    // insertDynamic: (data) => request("POST", `/dynamic`, data),
    // // 获取咨询师列表
    // getConsultPage: (data) => request("GET", `/consult/page/`, data),
};

module.exports = {
    API: API
}