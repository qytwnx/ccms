// const GET = 'GET';
// const POST = 'POST';
// const PUT = 'PUT';
// const FORM = 'FORM';
// const DELETE = 'DELETE';
const baseURL = 'http://localhost:8081/';
// const baseURL = 'https://www.qyt-code.top/api-xn-code';
export const serviceIp = baseURL
export default function request(method: any, url: string, data?: any) {
    return new Promise(function (resolve, reject) {
        let header = {
            'content-type': 'application/json;charset=utf-8',
            'Authorization': wx.getStorageSync('token') ? 'Bearer ' + wx.getStorageSync('token') : null
        };
        wx.request({
            url: baseURL + url,
            method: method,
            data: method === 'POST' ? JSON.stringify(data) : data,
            header: header,
            success(res:any) {
                if (res.data.code == 401) {
                    wx.removeStorageSync('token')
                    wx.removeStorageSync('userInfo')
                    wx.reLaunch({
                        url: '/pages/personal/percen'
                    })
                    wx.showToast({
                        title: res.data.msg,
                        icon: 'none',
                        duration: 1000
                    })
                } else {
                    resolve(res);
                }
            },
            fail(err:any) {
                reject(err)
            }
        })
    })
}
